import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Home} from './components/Home'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
 
function App() {
  return (
    <div className="container d-justify-content-center">
    <h1>Welcome to Contact Manager Portal</h1>
      <BrowserRouter>
        <Switch>
          <Route path='/' component={Home} exact></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;

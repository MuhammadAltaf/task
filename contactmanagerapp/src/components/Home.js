import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import { Button, ButtonToolbar } from 'react-bootstrap'
import { AddContactModal } from './AddContactModal'
import { EditContactModal } from './EditContactModal'
export class Home extends Component {

    constructor(props){
        super(props);
        this.state = {contacts :[], addModalShow : false, editModalShow: false}
    }

    componentDidMount(){
        this.refreshList();
    }

    componentDidUpdate(){
        this.refreshList();
    }

    refreshList(){
        fetch('http://localhost:54570/api/ContactManager')
        .then(response => response.json())
        .then(data => {
            this.setState({contacts:data})
        })
    }
    deleteContact(id){
        alert(id);
        if (window.confirm('Are you Sure? ')){
            fetch('http://localhost:54570/api/ContactManager/'+id,{
                method:'DELETE',
                header:{'Accept': 'Application/json',
                        'Content-Type': 'Application/json'
                }
            })
        }
    }
    render() {
            const {contacts,id,name,email,date_modified,date_created} = this.state;
            let addModalClose = () => this.setState({addModalShow:false})
            let editModalClose = () => this.setState({editModalShow:false})

            return (
                <div>
                <Table className="mt-4" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Contact Email</th>
                            <th>Date Modified</th>
                            <th>Date Created</th>
                            <th>Options</th>
                        </tr>   
                    </thead>
                    <tbody>
                        {
                            contacts.map(contact => (
                            <tr key = {contact.id}> 
                                <td>{contact.name}</td>
                                <td>{contact.Email}</td>
                                <td>{contact.DateModified}</td>
                                <td>{contact.DateCreated}</td>
                                <td>
                                    <ButtonToolbar>
                                        <Button
                                        className="mr-2" variant="info"
                                        onClick = { ()=>this.setState({editModalShow: true, 
                                                                        id: contact.ID, 
                                                                        name: contact.name, 
                                                                        email: contact.Email, 
                                                                        date_modified: contact.DateModified, 
                                                                        date_created: contact.DateCreated
                                                                    })
                                                  }
                                        >Edit
                                        </Button>
                                        <Button
                                        className="mr-2"
                                        onClick={ () => this.deleteContact(contact.ID)}
                                        variant="danger"
                                        >Delete
                                        </Button>

                                        <EditContactModal 
                                            show = {this.state.editModalShow}
                                            onHide = {editModalClose}
                                            id = {id}
                                            name = {name}
                                            email = {email}
                                            date_modified = {date_modified}
                                            date_created = {date_created}
                                        />
                                    </ButtonToolbar>
                                </td>
                            </tr>
                        ))
                        }
                    </tbody>
                </Table>
                <ButtonToolbar>
                    <Button variant="primary" 
                            onClick={() => this.setState({addModalShow:true})}
                            >Add Contact
                    </Button>
                <AddContactModal 
                        show = {this.state.addModalShow}
                        onHide = {addModalClose}
                />
                </ButtonToolbar>
            </div>
        )
    }
}
import React, {Component} from 'react'
import {Modal, Button, Col, Row, Form} from 'react-bootstrap'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'

export class EditContactModal extends Component{
    constructor(props){
        super(props);
        
        this.state = {snackbaropen: false, snackbarmsg: ''}
        this.handleSubmit = this.handleSubmit.bind(this);
        console.log(this.props)
    }
    snackbarClose = (event) => {
        this.setState({snackbaropen: false})
    }
    handleSubmit(event){
        event.preventDefault();
        fetch('http://localhost:54570/api/ContactManager',{
            method: 'PUT',
            headers: {
                'Accept': 'Application/json',
                'Content-Type': 'Application/json'
            },
            body:JSON.stringify({
                ID: event.target.ID.value,
                name: event.target.Name.value,
                Email: event.target.Email.value,
                DateModified: event.target.DateModified.value,
                DateCreated: event.target.DateCreated.value
            })
        })
        .then(response => response.json())
        .then((result) => {
            this.setState({snackbaropen: true, snackbarmsg: result});
        },
        (error)=>{
            this.setState({snackbaropen: true, snackbarmsg:'failed'});
        }
        )
    }

    render(){
        return(
            <div className="container">
            <Snackbar
            anchorOrigin= {{vertical: 'center', horizontal: 'center'}}
            open = {this.state.snackbaropen}
            autoHideDuration = {3000}
            onClose = {this.state.snackbarClose}
            message = {<span id="message-id">{this.state.snackbarmsg}</span>}
            action = {[
                <IconButton
                key = "close"
                aerial-label = "Close"
                color = "inherit"
                onClick = {this.snackbarClose}
                >
                 x
                </IconButton>
            ]}
            >
            </Snackbar>
            <Modal
            {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Contact
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
                <Row>
                    <Col sm={6}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId = "ID">
                                <Form.Label>ID</Form.Label>
                                <Form.Control
                                type="text"
                                name="ID"
                                required
                                disabled
                                defaultValue = {this.props.id}    
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group controlId = "Name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control
                                type="text"
                                name="Name"
                                required
                                defaultValue = {this.props.name}
                                placeholder="John"
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group controlId = "Email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                type="email"
                                name="Email"
                                required
                                defaultValue = {this.props.email}
                                placeholder="person@gmail.com"
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group controlId = "DateModified">
                                <Form.Label>Date Modified</Form.Label>
                                <Form.Control
                                type="date"
                                name="DateModified"
                                required
                                defaultValue = {this.props.date_modified}
                                placeholder="2019-01-31"
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group controlId = "DateCreated">
                                <Form.Label>Date Created</Form.Label>
                                <Form.Control
                                type="date"
                                name="DateCreated"
                                required
                                defaultValue = {this.props.date_created}
                                placeholder="2019-01-31"
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Button variant="primary" type="submit">Update Contact</Button>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={this.props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
    </div>
        ) 
    }
}